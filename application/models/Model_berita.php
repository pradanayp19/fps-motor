<?php

class Model_berita extends CI_Model {
	public function tampil_ber() 
	{
		return $this->db->get('tb_berita');
	}

	public function tambah_berita($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_berita($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_berita($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function find($id){
		$result = $this->db->where('id_berita', $id)
		->limit(1)
		->get('tb_berita');

		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}


	}

	public function detail_berita($id_berita)
	{
		$result = $this->db->where('id_berita', $id_berita)->get('tb_berita');
		if($result->num_rows() > 0){
			return $result->result();
		} else {
			return false;
		}
	}
}