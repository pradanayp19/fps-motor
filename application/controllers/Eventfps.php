<?php

class Eventfps extends MY_Controller
{
	public function index($page = null)
	{
		$data['title'] = 'Eventfps';
		$data['eventfps']    = $this->eventfps->paginate($page)->get();
		$data['total_rows'] = $this->eventfps->count();
		$data['pagination'] = $this->eventfps->makePagination(base_url('eventfps'), 2, $data['total_rows']);
		$data['page'] = 'event/event';
		$this->view2($data);
	}
}
