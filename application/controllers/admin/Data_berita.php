<?php 

class Data_berita extends CI_Controller {
	public function index() {
		$data['berita'] = $this->Model_berita->tampil_ber()->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/data_berita', $data);
		$this->load->view('templatead/footer');
	}

	public function tambah_berita() {
		$nama_berita							= $this->input->post('nama_berita');
		$kategori								= $this->input->post('kategori');
		$keterangan								= $this->input->post('keterangan');
		$tgl_berita								= $this->input->post('tgl_berita');
		$admin									= $this->input->post('admin');
		$gambar_berita							= $_FILES['gambar_berita']['name'];
		if ($gambar_berita =''){} else{
			$config ['upload_path'] = './upload';
			$config ['allowed_types'] = '*';
			$config ['max_size'] = 2000;

			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('gambar_berita')){
				echo "Gambar gagal di Upload!";
			} else{
				$gambar_berita=$this->upload->data('file_name');
			}

		} 

		$data = array(
			'nama_berita'								=> $nama_berita,
			'kategori'									=> $kategori,
			'keterangan'								=> $keterangan,
			'tgl_berita'									=> $tgl_berita,
			'admin'										=> $admin,
			'gambar_berita'								=> $gambar_berita
		);

		$this->Model_berita->tambah_berita($data, 'tb_berita');
		redirect('admin/data_berita/index');

	}

	public function detail($id_berita)
	{
		$data['berita'] = $this->Model_berita->detail_berita($id_berita);
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/detail_berita', $data);
		$this->load->view('templatead/footer');
	}

	public function edit($id)
	{
		$where = array('id_berita' => $id);
		$data['berita'] = $this->Model_berita->edit_berita($where, 'tb_berita')->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/edit_berita', $data);
		$this->load->view('templatead/footer');
	}

	public function update_berita(){
		$id								= $this->input->post('id_berita');
		$nama_berita					= $this->input->post('nama_berita');
		$kategori						= $this->input->post('kategori');
		$keterangan						= $this->input->post('keterangan');
		$tgl_berita						= $this->input->post('tgl_berita');
		$admin							= $this->input->post('admin');

		$data = array(
			'nama_berita'									=> $nama_berita,
			'kategori'										=> $kategori,
			'keterangan'									=> $keterangan,
			'tgl_berita'									=> $tgl_berita,
			'admin'											=> $admin,
			'gambar_berita'									=> $gambar_berita
		);	

		$where = array(
			'id_berita' => $id

		);

		$this->Model_berita->update_berita($where, $data, 'tb_berita');
		redirect('admin/data_berita/index');

	}

	public function hapus($id){

		$where = array('id_berita' => $id);
		$this->Model_berita->hapus_data($where, 'tb_berita');
		redirect('admin/data_berita/index');

	}

}

?>