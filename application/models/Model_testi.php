<?php

class Model_testi extends CI_Model {
	public function tampil_test() 
	{
		return $this->db->get('tb_testi');
	}

	public function tambah_testi($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_testi($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_testi($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table){
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function find($id){
		$result = $this->db->where('id_testi', $id)
		->limit(1)
		->get('tb_testi');

		if($result->num_rows() > 0){
			return $result->row();
		}else{
			return array();
		}


	}

	public function detail_testi($id_testi)
	{
		$result = $this->db->where('id_testi', $id_testi)->get('tb_testi');
		if($result->num_rows() > 0){
			return $result->result();
		} else {
			return false;
		}
	}
}