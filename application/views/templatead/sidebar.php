 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
           <img src="<?= base_url() ?>assets/admin/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
           <span class="brand-text font-weight-light">FPS MOTOR</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
           <!-- Sidebar user panel (optional) -->
           <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                     <img src="<?= base_url() ?>assets/admin/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                     <a href="#" class="d-block">Admin</a>
                </div>
           </div>

           <!-- SidebarSearch Form -->
           <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                     <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                     <div class="input-group-append">
                          <button class="btn btn-sidebar">
                               <i class="fas fa-search fa-fw"></i>
                          </button>
                     </div>
                </div>
           </div>

           <!-- Sidebar Menu -->
           <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                     <!-- Add icons to the links using the .nav-icon class
          	with font-awesome or any other icon font library -->
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/dashboard') ?>" class="nav-link <?= activate_menu('dashboard') ?>">
                               <i class="nav-icon fas fa-tachometer-alt"></i>
                               <p>
                                    Dashboard
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/category') ?>" class="nav-link <?= activate_menu('category') ?>">
                               <i class="nav-icon fas fa-th"></i>
                               <p>
                                    Category
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/product') ?>" class="nav-link <?= activate_menu('product') ?>">
                               <i class="nav-icon fas fa-th"></i>
                               <p>
                                    Produk
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/event') ?>" class="nav-link <?= activate_menu('event') ?>">
                               <i class="nav-icon fas fa-users"></i>
                               <p>
                                    Event
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/data_showroom') ?>" class="nav-link <?= activate_menu('data_showroom') ?>">
                               <i class="nav-icon fas fa-envelope-open-text"></i>
                               <p>
                                    Showroom
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/data_berita') ?>" class="nav-link <?= activate_menu('data_berita') ?>">
                               <i class="nav-icon fas fa-copy"></i>
                               <p>
                                    Berita
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/kontak') ?>" class="nav-link <?= activate_menu('kontak') ?>">
                               <i class="far fa-circle nav-icon"></i>
                               <p>
                                    Kontak Kami
                               </p>
                          </a>
                     </li>
                     <li class="nav-item menu-open">
                          <a href="<?= base_url('admin/data_testi') ?>" class="nav-link <?= activate_menu('data_testi') ?>">
                               <i class="far fa-circle nav-icon"></i>
                               <p>
                                    Testimoni
                               </p>
                          </a>
                     </li>
                </ul>
           </nav>
           <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
 </aside>