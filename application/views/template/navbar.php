<body>
	<div id="wrapper" class="wrapper clearfix">
		<header id="navbar-spy" class="header header-1">
			<div class="top-bar">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-5">
							<ul class="list-inline top-contact">
								<li><span>Phone :</span> + 2 0106 5370701</li>
								<li><span>Email :</span> <a href="https://demo.zytheme.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="ba8dd5c8d5d5dcfa8dd5c8d5d5dc94d9d5d7">[email&#160;protected]</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-7">
							<ul class="list-inline pull-right top-links">
								<li>
									<a href="#">Login</a>
									/
									<a href="#">Daftar</a>
								</li>
								<li>
									<a href="#">Wishlist</a>
								</li>
								<li>
									<a href="#">Checkout</a>
								</li>
								<li>
									<a href="#">Stores</a>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">English <i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu">
										<li>
											<a href="#">Arabic</a>
										</li>
										<li>
											<a href="#">German</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>

				</div>

			</div>

			<nav id="primary-menu" class="navbar navbar-fixed-top">
				<div class="container">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="logo" href="index.html">
							<img src="<?= base_url() ?>assets/images/logofps/Logofps.png" alt="Autoshop">
						</a>
					</div>

					<div class="collapse navbar-collapse pull-right" id="header-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-left">
							<li class="has-dropdown">
								<a href="<?php echo base_url('Dashboard') ?>">Beranda</a>
							</li>

							<li class="has-dropdown">
								<a href="<?php echo base_url('Showroom') ?>">Showroom</a>

							</li>

							<li>
								<a href="<?php echo base_url('Shop') ?>">Shop</a>
							</li>
							<li class="has-dropdown">
								<a href="<?php echo base_url('Eventfps') ?>">Event</a>
							</li>

							<li class="has-dropdown">
								<a href="<?php echo base_url('Berita') ?>">Berita</a>
							</li>
							<li class="has-dropdown pull-left">
								<a href="<?php echo base_url('Tentang') ?>">Tentang Kami</a>
							</li>

						</ul>

						<div class="module module-search pull-left">
							<div class="search-icon">
								<i class="fa fa-search"></i>
								<span class="title">Cari</span>
							</div>
							<div class="search-box">
								<form class="search-form">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Type Your Search Words">
										<span class="input-group-btn">
											<button class="btn" type="button"><i class="fa fa-search"></i></button>
										</span>
									</div>

								</form>
							</div>
						</div>


						<div class="module module-cart pull-left">
							<div class="cart-icon">
								<i class="fa fa-shopping-cart"></i>
								<span class="title">shop cart</span>
								<span class="cart-label">0</span>
							</div>
						</div>

					</div>

				</div>

			</nav>
		</header>