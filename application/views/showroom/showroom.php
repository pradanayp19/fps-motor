<section id="page-title" class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<h1>SHOWROOM FPS MOTOR</h1>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6">
				<ol class="breadcrumb text-right">
					<li>
						<a href="index.html">Home</a>
					</li>
					<li class="active">grid</li>
				</ol>
			</div>

		</div>

	</div>

</section>


<section id="shopgrid" class="shop shop-grid">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				<div class="row">
					<div class="col-xs-12  col-sm-12  col-md-12">
						<div class="shop-options">
							<div class="product-options2 pull-left pull-none-xs">
								<ul class="list-inline">
									<li>
										<div class="product-sort mb-15-xs">
											Kategori : <strong><?= isset($category) ? $category : 'Semua Kategori' ?></strong>

										</div>
									</li>

								</ul>
							</div>

							<div class="product-view-mode text-right pull-none-xs">
								<span class="float-right">
									Urutkan Harga : <a href="<?= base_url("/shop/sortby/asc") ?>" class="badge badge-primary">Termurah</a> | <a href="<?= base_url("/shop/sortby/desc") ?>" class="badge badge-primary">Termahal</a>
								</span>
							</div>

						</div>

					</div>

				</div>

				<div class="row">
					<?php foreach ($showroom as $show) : ?>
						<div class="col-xs-12 col-sm-6 col-md-4 product">
							<div class="product-img">
								<img src="<?php echo $show->image ? base_url("/images/product/$show->image") : base_url("images/product/default.jpg")  ?>" alt="" width="200px" height="150px" />
								<div class="product-hover">
									<div class="product-action">
										<a class="btn btn-primary" href="<?php echo base_url("showroom/detail_showroom/$show->id") ?>">Item Details</a>
									</div>
								</div>

							</div>

							<div class="product-bio">
								<div class="prodcut-cat">
									<a href="<?= base_url('showroom/showroom_detail' . $show->id) ?>"><?= $show->category_title ?></a>
								</div>

								<div class="prodcut-title">
									<h3>
										<a href="<?= base_url('showroom/showroom_detail' . $show->id) ?>"><?= $show->product_title ?></a>
									</h3>
								</div>

								<div class="product-price">
									<span class="symbole">Rp. <?= number_format($show->price), 0, ',', '.'  ?></span>
								</div>

							</div>

						</div>

					<?php endforeach ?>

				</div>
				<nav aria-label="Page navigation example">
					<?= $pagination ?>
				</nav>

			</div>

			<div class="col-md-3">
				<div class="search-icon">
					<i class="fa fa-search"></i>
					<span class="title">Pencarian</span>
				</div>
				<div class="search-box active">
					<form class="search-form">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Input Your Keys">
							<span class="input-group-btn">
								<button class="btn" type="submit"><i class="fa fa-search"></i></button>
							</span>
						</div>

					</form>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-3 sidebar">

				<div class="widget widget-categories">
					<div class="widget-title">
						<h5>categories</h5>
					</div>

					<div class="widget-content">
						<ul class="list-unstyled">
							<li>
								<a href="#"><i class="fa fa-angle-double-right"></i>Motor Baru<span>(5)</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-angle-double-right"></i>Motor Bekas<span>(77)</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-angle-double-right"></i>HELM<span>(6)</span></a>
							</li>
							<li>
								<a href="#"><i class="fa fa-angle-double-right"></i>Aksesories<span>(11)</span></a>
							</li>

						</ul>
					</div>
				</div>

				<div class="widget widget-tags">
					<div class="widget-title">
						<h5>tag clouds</h5>
					</div>
					<div class="widget-content">
						<a href="#">responsive</a>
						<a href="#">modern</a>
						<a href="#">corporate</a>
						<a href="#">business</a>
						<a href="#">fresh</a>
						<a href="#">awesome</a>
						<a href="#">business</a>
						<a href="#">fresh</a>
						<a href="#">corporate</a>
						<a href="#">autoshop</a>
					</div>
				</div>

			</div>

		</div>

	</div>

</section>