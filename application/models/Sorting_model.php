<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sorting_model extends MY_Model
{

    protected $table    = 'product';
    protected $perPage  = 3;
}

/* End of file Sorting_model.php */