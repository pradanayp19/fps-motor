<?php

class Dashboard extends MY_Controller
{

	public function index($page = null)
	{
		$data['title'] = 'Homepage';
		$data['produk']    = $this->dashboard->select(
			[
				'product.id', 'product.title AS product_title', 'product.description', 'product.image', 'product.price', 'product.is_available', 'category.title AS category_title', 'category.slug AS category_slug'
			]
		)
			->join('category')
			->where('product.is_available', 1)
			->get();

		$data['total_rows'] = $this->dashboard->where('product.is_available', 1)->count();

		$data['page'] = 'dashboard';
		$this->view2($data);
	}
}
