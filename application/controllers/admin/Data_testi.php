<?php 

class Data_testi extends CI_Controller {
	public function index() {
		$data['testi'] = $this->Model_testi->tampil_test()->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/data_testi', $data);
		$this->load->view('templatead/footer');
	}
	
	public function tambah_testi() {
		$nama									= $this->input->post('nama');
		$profesi								= $this->input->post('profesi');
		$komentar_testi							= $this->input->post('komentar_testi');
		$gambar_testi							= $_FILES['gambar_testi']['name'];
		if ($gambar_testi =''){} else{
			$config ['upload_path'] = './upload';
			$config ['allowed_types'] = '*';
			$config ['max_size'] = 2000;

			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('gambar_testi')){
				echo "Gambar gagal di Upload!";
			} else{
				$gambar_testi=$this->upload->data('file_name');
			}

		} 

		$data = array(
			'nama'								=> $nama,
			'profesi'							=> $profesi,
			'komentar_testi'					=> $komentar_testi,
			'gambar_testi'						=> $gambar_testi
		);

		$this->Model_testi->tambah_testi($data, 'tb_testi');
		redirect('admin/data_testi/index');

	}

	public function detail($id_testi)
	{
		$data['testi'] = $this->Model_testi->detail_testi($id_testi);
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/detail_testi', $data);
		$this->load->view('templatead/footer');
	}

	public function edit($id)
	{
		$where = array('id_testi' => $id);
		$data['testi'] = $this->Model_testi->edit_testi($where, 'tb_testi')->result();
		$this->load->view('templatead/header');
		$this->load->view('templatead/navbar');
		$this->load->view('templatead/sidebar');
		$this->load->view('admin/edit_testi', $data);
		$this->load->view('templatead/footer');
	}

	public function update_testi(){
		$id								= $this->input->post('id_testi');
		$nama							= $this->input->post('nama');
		$profesi						= $this->input->post('profesi');
		$komentar_testi					= $this->input->post('komentar_testi');

		$data = array(
			'nama'									=> $nama,
			'profesi'								=> $profesi,
			'komentar_testi'						=> $komentar_testi,
			'gambar_testi'							=> $gambar_testi
		);	

		$where = array(
			'id_testi' => $id

		);

		$this->Model_testi->update_testi($where, $data, 'tb_testi');
		redirect('admin/data_testi/index');

	}

	public function hapus($id){

		$where = array('id_testi' => $id);
		$this->Model_testi->hapus_data($where, 'tb_testi');
		redirect('admin/data_testi/index');

	}

}

?>