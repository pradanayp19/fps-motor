<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Eventfps_model extends MY_Model
{

    protected $table    = 'event';
    protected $perPage  = 1;
}

/* End of file Eventfps_model.php */