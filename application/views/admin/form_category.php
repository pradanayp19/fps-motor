<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Data Produk</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Data Produk</a></li>
                        <li class="breadcrumb-item active">Edit Data Produk</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30 mt-3">
                        <div class="card-body">
                            <?= form_open($form_action, ['method' => 'POST']) ?>
                            <?= isset($input->id) ? form_hidden('id', $input->id) : '' ?>
                            <div class="form-group">
                                <label for="">Kategori</label>
                                <?= form_input('title', $input->title, ['class' => 'form-control', 'id' => 'title', 'onkeyup' => 'createSlug()', 'required' => true, 'autofocus' => true]) ?>
                                <?= form_error('title') ?>
                            </div>
                            <div class="form-group">
                                <label for="">Slug</label>
                                <?= form_input('slug', $input->slug, ['class' => 'form-control', 'id' => 'slug', 'required' => true]) ?>
                                <?= form_error('slug') ?>
                            </div>

                            <button type="submit" class="btn btn-primary">Simpan</button>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>