<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Data berita</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Data berita</a></li>
						<li class="breadcrumb-item active">Edit Data berita</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<div class="card m-b-30 mt-3">
						<div class="card-body">
							<?php foreach ($berita as $ber) : ?>

								<form action="<?= base_url() . 'admin/data_berita/update_berita' ?>" method="post" enctype="multipart/form-data">

									<div class="form-group">
										<label>Nama berita</label>
										<input type="hidden" name="id_berita" class="form-control" value="<?= $ber->id_berita ?>">
										<input type="text" name="nama_berita" class="form-control" value="<?= $ber->nama_berita ?>" min="0">
									</div>

									<div class="form-group">
										<label>Admin</label>
										<input type="text" name="admin" class="form-control" value="<?= $ber->admin ?>">
									</div>

									<div class="form-group">
										<label>Tanggal</label>
										<input type="text" name="tgl_berita" class="form-control" value="<?= $ber->tgl_berita ?>">
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Kategori</label>
											<select class="form-control" name="kategori" value="<?= $ber->kategori ?>"required>
												<option>- Pilih Kategori -</option>
												<option value="berita_baru">berita Baru</option>
												<option value="berita_akan_datang">berita Akan Datang</option>
												<option value="berita_berjalan">berita Berjalan</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label>Keterangan</label>
										<textarea id="summernote" type="text" name="keterangan" class="form-control" placeholder="Tulis di sini......" required>
											<?= $ber->keterangan ?>
										</textarea>
									</div>

									<button type="submit" class="btn btn-primary waves-effect waves-light mt-3 col-12">Simpan</button>
								</form>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>