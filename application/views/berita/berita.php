<section id="page-title" class="page-title">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
				<h1>BERITA</h1>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6">
				<ol class="breadcrumb text-right">
					<li>
						<a href="index.html">Home</a>
					</li>
					<li class="active">Berita</li>
				</ol>
			</div>

		</div>

	</div>

</section>


<section id="blog" class="blog blog-grid">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/1.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/2.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-youtube-play"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/3.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/4.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/5.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/6.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/7.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/8.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/9.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/10.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/11.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>


			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="entry mb-30">
					<div class="entry-img">
						<a href="#">
							<img src="<?= base_url() ?>assets/images/blog/grid/12.jpg" alt="Entry Title">
						</a>
					</div>

					<div class="entry-content">
						<div class="entry-format">
							<i class="fa fa-image"></i>
						</div>
						<div class="entry-title">
							<h3>
								<a href="#">Framing and Insulating Walls In Warehouse and Corporate</a>
							</h3>
						</div>

						<ul class="entry-meta list-inline clearfix">
							<li class="entry-date">On: <span>Feb 12, 2015</span>
							</li>
							<li class="entry-author">By:
								<a href="#">Begha</a>
							</li>
							<li class="entry-num-comments">Comments:
								<a href="#">45</a>
							</li>
						</ul>

						<div class="entry-snippet">
							<p>Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing, Duis dapibus aliquam mi, et euismod scelerisque ut. Vivamus elit quis urna adipiscing ...</p>
						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12  text-center">
				<ul class="pagination">
					<li class="active">
						<a href="#">1</a>
					</li>
					<li>
						<a href="#">2</a>
					</li>
					<li>
						<a href="#">3</a>
					</li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
						</a>
					</li>
				</ul>
			</div>

		</div>
	</div>

</section>