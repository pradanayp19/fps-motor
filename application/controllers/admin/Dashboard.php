<?php

class Dashboard extends MY_Controller
{
	public function index($page = null)
	{
		$data['title']      = 'Dashboard';
		$data['page']       = 'admin/dashboard';

		$this->view($data);
	}
}
